import { NgModule } from '@angular/core';
import { BuscarPersonajePipe } from './buscar-personaje/buscar-personaje';
@NgModule({
	declarations: [BuscarPersonajePipe],
	imports: [],
	exports: [BuscarPersonajePipe]
})
export class PipesModule {}
