import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable()
export class ServicesProvider {


    //Declaracion de variables
    constructor(public http: HttpClient) {
    }



    /*Obtiene  la lista  de comics */
    public obtenerListaComics(fin){
        return this.http.get('https://gateway.marvel.com:443/v1/public/comics?noVariants=true&orderBy=focDate&limit=25&offset='+fin+'&apikey=25d4f49ab8aebd16ae9b3ae4b24584ae')
    }
    /*Obtiene  el detalle de un comic mediante un ID */
    public detalleComid(id){
        return this.http.get('https://gateway.marvel.com:443/v1/public/comics/'+id+'?apikey=25d4f49ab8aebd16ae9b3ae4b24584ae')
    }
    /*Obtiene  la lista  de PERSONAJE de un un comid mediante un ID*/
    public obtenerPersonaje(id){
        return this.http.get('https://gateway.marvel.com:443/v1/public/comics/'+id+'/characters?apikey=25d4f49ab8aebd16ae9b3ae4b24584ae')
    }
}
