import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServicesProvider} from './../../providers/services/services'
import { Observable } from 'rxjs';
import { HomePage} from './../home/home'
import 'rxjs/add/operator/map'


@IonicPage()
@Component({
  selector: 'page-detalle',
  templateUrl: 'detalle.html',
})
export class DetallePage {
  //Declaracion de Variables
    idComic:any;
    objetoComic:Observable<any>;
    personajes:Observable<any>;
    constructor(
        public navCtrl: NavController, 
        public servicios: ServicesProvider,
        public navParams: NavParams
    ) {
          //Obtiene los paremetros enviados 
        this.idComic = navParams.get('datos');
          //Realiza la consulta mediante un observable del comic segun su ID
        this.objetoComic = this.servicios.detalleComid( this.idComic ).map(r=>{
            return r['data']['results'][0];
        })
          //Obtiene un observable con los personajes del comic
        this.personajes = this.servicios.obtenerPersonaje(this.idComic).map(personaje=>{
            return personaje['data']['results']
        })
  }
  //Regrega a la pantalla inicial
    regresar() {
        this.navCtrl.setRoot(HomePage)
    }

}
