import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ServicesProvider} from './../../providers/services/services'
import { Observable} from 'rxjs'
import { DetallePage} from './../detalle/detalle'
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  //Declaracion de Variables
  listadoComics=[];
  obj:any;
  inicio = 0;
  fin = 0;
  totalData = 0;
  //Declaracion de Variables
  constructor(
    public navCtrl: NavController,
    public servicios: ServicesProvider) {
      

  }

    ngOnInit(): void {
        //Inicia el proceso de carga  de comics con 20 elementos
        this.servicios.obtenerListaComics(0).subscribe(res=>{
            this.obj = res;
            for (var i = 0; i < 20; i++) {
                //Se añade al objeto la propiedad de img que hace referencia a la portada del comic
                let objeto = this.obj.data.results[i];
                objeto.img=this.obj.data.results[i].thumbnail.path +'.jpg'
                this.listadoComics.push( objeto);
            }
            this.fin =0
        });
    }
    


    //Funncion a ejecutar cuando se realiza el scroll
    //Obtiene los elementos segun corresponda
    doInfinite(infiniteScroll) {
        this.fin +=20
        setTimeout(() => {
            this.servicios.obtenerListaComics(this.fin).subscribe(res=>{
                this.obj = res;
                for (var i = 0;  i <  25  ; i++) {
                    let objeto = this.obj.data.results[i];
                    objeto.img=this.obj.data.results[i].thumbnail.path +'.jpg'
                    this.listadoComics.push( objeto);
                }
            infiniteScroll.complete();
            });
        }, 500);
    }
    //Redireciona a la pantalla de detalle con los parametros ID del comic elegido
    detalleComic(id){
        this.navCtrl.setRoot(DetallePage, {
            datos: id
        })
    }

}
