import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpClientModule} from '@angular/common/http'
  //Declaracion de Paginas 
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { DetallePage} from './../pages/detalle/detalle'
  //Declaracion de Servicios
import { ServicesProvider } from '../providers/services/services';
  //Declaracion de Pipes
import { BuscarPersonajePipe} from './../pipes/buscar-personaje/buscar-personaje'
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    DetallePage,
    BuscarPersonajePipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    DetallePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    HttpClientModule,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ServicesProvider
  ]
})
export class AppModule {}
